<?php

require 'src/UserFile.php';

$file_user = new UserFile();
$list_emails = $file_user->listEmails();

?>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>CI&T - API</title>
</head>

<body>



<h2>REST API</h2>

<ul>
<li>GET /api/user : listar</li>
<li>POST /api/user : criar</li>
<li>POST /api/user + _method "put" : atualizar</li>
<li>POST /api/user + _method "delete" : deletar</li>
<li>"user" pode ser passado via POST em campo "object"</li>
</ul>


<ul style="    display: block;
    width: 450px;
    margin: 0px auto;">
 
    <li><a href="list_users.php" target="_blank">Arquivo</a></li>
    <li>
<div>
<h2>Adicionar um usuario novo</h2>
<form action="api/user" method="POST">
<input type="hidden" name="object" value="user" />
<div>
<label> Nome
<input type="text" name="name" required>
</label>
</div>
<div>
<label> Sobrenome
<input type="text" name="surname" required>
</label>
</div>
<div>
<label> E-mail
<input type="email" name="email" required>
</label>
</div>
<div>
<label> Telefone
<input type="text" name="phone" required>
</label>
</div>
<button type="submit">Salvar</button>
</form>
</div>
</li>
<li>
<div>
<h2>Atualizar os dados de um usuario</h2>
<form action="api/user" method="POST">
<input type="hidden" name="object" value="user" />
<input type="hidden" name="_method" value="put" />
<div>
<label> Nome
<input type="text" name="name" required>
</label>
</div>
<div>
<label> Sobrenome
<input type="text" name="surname" required>
</label>
</div>
<div>
<label> E-mail
<select name="email" required>
<option value="">Selecione</option>
<?php foreach($list_emails as $k => $mail): ?>
<option><?php echo $mail; ?></option>
<?php endforeach; ?>
</select>
</label>
</div>
<div>
<label> Telefone
<input type="text" name="phone" required>
</label>
</div>
<button type="submit">Salvar</button>
</form>
</div>
</li>
<li>
<div>
<h2>Deletar usuarios por email</h2>
<form action="api/user" method="POST">
<input type="hidden" name="object" value="user" />
<input type="hidden" name="_method" value="delete" />
<div>
<label> Email
<select name="email" required>
<option value="">Selecione</option>
<?php foreach($list_emails as $k => $mail): ?>
<option><?php echo $mail; ?></option>
<?php endforeach; ?>
</select>


</label>

</div>
<button type="submit" style="float:right;">Deletar</button>
</form>
</li>


</ul>



</body>

</html>