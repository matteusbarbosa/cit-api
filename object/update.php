<?php

$data = [];


foreach($_POST as $k => $v){
    $data[$k] = filter_var($v, FILTER_SANITIZE_SPECIAL_CHARS);
}

$class_name = ucwords($data['object']);

if(!file_exists($_SERVER['DOCUMENT_ROOT'].'/model/'.$class_name.'.php'))
throw new \Exception('Classe inexistente');

require $_SERVER['DOCUMENT_ROOT'].'/model/'.$class_name.'.php';

$object = new $class_name();
$object->setInput($data);
$success = $object->update($data);

if($success)
echo 'Cadastro atualizado. <a href="/">Início</a>';
else
echo "FAIL";
