

<?php

$class_name = ucwords($object).'File';

require_once 'src/'.$class_name.'.php';

$file = new $class_name();
$list = array_reverse($file->listAll());

print_r(json_encode($list));