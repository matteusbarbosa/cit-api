<?php

$url = $_SERVER['REQUEST_URI'];
$parts = parse_url($url);

if($parts)
$path_pieces = explode('/', $parts['path']);

$object = isset($path_pieces[2]) ? $path_pieces[2] : null;
$object_action = isset($path_pieces[3]) ? $path_pieces[3] : null;

//$query = parse_str($parts['query']);
//if($query)
//echo $query['q'];

//object/action

if($object == null && !isset($_POST['object']))
throw new \Exception("Especifique o POST object");

if($_POST == null){
    require 'object/list.php';
    exit;
}

if(!isset($_POST['_method'])){
    require 'object/create.php';
    exit;
}

switch($_POST['_method']){
    case "put":
    require 'object/update.php';
    break;
    case "delete":
    require 'object/delete.php';
    break;
}
