

<?php

include 'src/UserFile.php';


$file = new UserFile();
$list = array_reverse($file->listAll());

?>

<table border="1" width="100%">
  <thead>
    <tr>
      <th>Nome</th>
      <th>Sobrenome</th>
      <th>E-mail</th>
      <th>Telefone</th>
    </tr>
  </thead>
  <tbody>

  <?php foreach($list as $k => $user): ?>
  <tr>
  <td><?php echo $user['name']; ?></td>
  <td><?php echo $user['surname']; ?></td>
  <td><?php echo $user['email']; ?></td>
  <td><?php echo $user['phone']; ?></td>
  </tr>
<?php endforeach; ?>
  
  </tbody>
</table>