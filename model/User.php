<?php

require $_SERVER['DOCUMENT_ROOT'].'/src/UserFile.php';

class User{

    private  $input, $name, $surname, $email, $phone;

    /**
     * Get the value of name
     */ 
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set the value of name
     *
     * @return  self
     */ 
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get the value of surname
     */ 
    public function getSurname()
    {
        return $this->surname;
    }

    /**
     * Set the value of surname
     *
     * @return  self
     */ 
    public function setSurname($surname)
    {
        $this->surname = $surname;

        return $this;
    }

    /**
     * Get the value of email
     */ 
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set the value of email
     *
     * @return  self
     */ 
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get the value of phone
     */ 
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Set the value of phone
     *
     * @return  self
     */ 
    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * Get the value of input
     */ 
    public function getInput()
    {
        return $this->input;
    }

    /**
     * Set the value of input
     *
     * @return  self
     */ 
    public function setInput($input)
    {
        $this->name = $input['name'];
        $this->surname = $input['surname'];
        $this->email = $input['email'];
        $this->phone = $input['phone'];

        return $this;
    }

    public function update(){
        $file = new UserFile();
        $file->update($this->name, $this->surname, $this->email, $this->phone);
        return true;
    }

    public function write(){
        $file = new UserFile();
        $file->insert($this->name, $this->surname, $this->email, $this->phone);
        return true;
    }

    public function delete($email){
        $file = new UserFile();
        $file->delete($email);
        return true;
    }
}