<?php


class File{

    private $file_path;

    public function __construct($file_path){
        $this->setFilePath($file_path);
    }

    public function setFilePath($file_path){
        $this->file_path = $file_path;
    }

    public function getFilePath(){
        return $this->file_path;
    }
}