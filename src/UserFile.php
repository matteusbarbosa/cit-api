<?php

require 'File.php'; 

class UserFile extends File {
    
    private $file_name = 'users.txt';
    
    public function __construct(){
        $this->setFilePath($_SERVER['DOCUMENT_ROOT'].'/data/'.$this->file_name);
    }
    
    public function listAll(){
        $handle = fopen($this->getFilePath(), "r") or die("Arquivo não encontrado");
        
        if ($handle) {
            while (($line = fgets($handle)) !== false) {
                $data[] = explode(',', $line);
            }  
            $data = array_map(function($tag) {
                return array(
                    'name' => $tag[0],
                    'surname' => $tag[1],
                    'email' => $tag[2],
                    'phone' => $tag[3]
                );
            }, $data);
            
            fclose($handle);
        }
        
        return $data;
        
    }
    public function listEmails(){
        $handle = fopen($this->getFilePath(), "r") or die("Arquivo não encontrado");
        
        if ($handle) {
            while (($line = fgets($handle)) !== false) {
                $data[] = explode(',', $line);
            }  
            $data = array_map(function($tag) {
                return $tag[2];
            }, $data);
            
            fclose($handle);
        }
        rsort($data);
        
        return $data;
        
    }
    public function update($name, $surname, $email, $phone){

        $line_no = 0;
        $fname = $this->getFilePath(); 
        $lines = file($fname);
        $out = '';
        foreach($lines as $line){
            $row = explode(',', $line);
            $line_no++;    
            if($row[2] == $email)
            $out .= $name.",".$surname.",".$email.",".$phone.PHP_EOL;
            else
            $out .= $line;
        }
        $f = fopen($fname, "w"); 
        fwrite($f, $out); 
        fclose($f); 
        return true;
    }


    public function insert($name, $surname, $email, $phone){
        
        $myfile = fopen($this->getFilePath(), "a") or die("Arquivo não encontrado");
        $row = $name.",".$surname.",".$email.",".$phone.PHP_EOL;
        fwrite($myfile, $row);
        fclose($myfile);
        
        return $email;
    }

    public function delete($email){
       $line_no = 0;
        $fname = $this->getFilePath(); 
        $lines = file($fname);
        $out = '';
        foreach($lines as $line){
            $row = explode(',', $line);
            $line_no++;            
            if($row[2] != $email)
            $out .= $line;
        }
        $f = fopen($fname, "w"); 
        fwrite($f, $out); 
        fclose($f); 
        return true;
    }
    
}